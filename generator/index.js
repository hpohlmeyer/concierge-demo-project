const Metalsmith = require('metalsmith');
const concierge = require('metalsmith-concierge');
const layout = require('metalsmith-layouts');

Metalsmith(__dirname)
  .source(process.env.CONCIERGE_CONTENT_DIR)
  .destination(process.env.CONCIERGE_BUILD_DIR)
  .clean(true)
  .ignore('.gitkeep')
  .use(concierge(process.env.CONCIERGE_CONFIG_PATH, 'ejs'))
  .use(layout({
    default: 'default.ejs',
    pattern: '**/*.html',
  }))
  .build(error => {
    if (error) { throw error; }
    console.log('Build succeeded.');
  });
